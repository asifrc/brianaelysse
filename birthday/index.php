<?php //Bismillah
include('head.php');
?>
	<div id="main-carousel" class="csel carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
		<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-generic" data-slide-to="1"></li>
		<li data-target="#carousel-example-generic" data-slide-to="2"></li>
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner">
		<div class="item active">
		  <div class="carousel-caption visible-md visible-lg text-center" style="position: fixed">
			  <h3>Upload Your Favorite Memories of Your Favorite Person for her Birthday!!</h3>
			  <p>&nbsp;</p>
			  <button class="btn btn-lg btn-primary btnsignup">Sign in with Facebook</button>
			  <p>&nbsp;</p>
		  </div>
		  <img src="./images/profilepic01.jpg" width="100%"  style="max-height: 100%" alt="See Bree Laugh">
		</div>
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>
	<div class="csel visible-xs visible-sm text-center">
		<h3>Upload Your Favorite Memories of Your Favorite Person for her Birthday!!</h3>
		<button class="btn btn-lg btn-primary btnsignup">Sign in with Facebook</button>
	</div>

<div id="pinboard" class="container center-block">
  <div id="memories"></div>
</div>

<div id="uploadform" class="container center-block">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<h3>Upload a Memory</h3>
		<form role="form">
      <div id="formAlert"></div>
			<div class="form-group">
        <label for="photo">Pick a Photo</label>
        <div>
          <span class="btn btn-sm btn-primary fileinput-button">
            <i class="glyphicon glyphicon-arrow-up"></i>
            <span>Upload a file</span>
				    <input type="file" id="photo" data-url="uploads/">
          </span>
				  <span>or</span>
				  <button id="btnfbchoose" class="btn btn-sm btn-primary">Choose from Facebook</button>
        </div>
			</div>
			<div class="form-group">
				<label for="note">Write a Note</label>
				<textarea id="note" placeholder="Write something.. Warm and fuzzies encouraged :)"></textarea>
			</div>
			<button id="btnUpload" class="btn btn-success">Upload</button>
			<button id="btnDelete" data-toggle="modal" data-target="#dvErase" class="btn btn-danger pull-right hidden">Delete</button>
		</form>
		<div class="preview polaroid">
			<img id="imgPrev" width="100%">
			<div id="notePrev"></div>
	</div>
</div>
<div class="modal fade" id="dvErase" tabindex="-1" role="dialog" aria-labelledby="EraseMemory" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">x</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Erase Memory?</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this memory?</p>
      </div>
      <div class="modal-footer">
			  <button id="btnErase" class="btn btn-danger">Delete</button>
			  <button data-dismiss="modal" data-target="#dvErase" class="btn btn-default">Cancel</button>
      </div>
    </div>
  </div>
</div>
<?php
include('fbphotoselector.php');
?>
<script type="text/javascript" src="js/index.js"></script>
<?php
include('foot.php');
?>
