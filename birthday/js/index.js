var fbuser = {};

// On "Sign In with Facebook" click
$('.btnsignup').on('click', function() {
  loginToFB(function() {
	  showPage('newMemory');
  });
});

var loginToFB = function(cb) {
	//Popup facebook login dialog
	FB.login(function(response) {
		if (response.authResponse)
		{
			FB.api('/me', function(resp) {
        fbuser = resp;
        $.cookie.json = true;
        $.cookie('fbuser', fbuser);
        updateNav();
        if (typeof cb === "function") {
          cb();
        }
			});
		}
  }, {scope: 'email,user_photos,friends_photos'});
};

var Memory = function(data) {
  var self = this;
  data = data || {};
  var defaultValues = {
    id: -1,
    copyFile: false,
    photo: "",
    note: "",
    user: fbuser
  };
  for (var field in defaultValues) {
    self[field] = data[field] || defaultValues[field];
  }
  self.note = self.note.replace(/&lt;/g, "<").replace(/&gt;/g, ">");
  self.note = self.note.replace(/<br>/g, "");
};

var Upload = function() {
  var self = this;

  var memory = new Memory();

  self.init = function() {
    // Hide form upload on init
    $('#uploadform, #pinboard, .csel, .csel *').hide();
    $('#formAlert').hide();
    //jQuery File Uplaod
    $('#photo').fileupload({
      dataType: 'json',
      done: function (e, data) {
        $.each(data.result.files, function (index, file) {
          $('#imgPrev').attr('src', file.url);
          memory.copyFile = false;
          memory.photo = file.url;
        });
      }
    });
    // On "Choose from Facebook" click
    $('#btnfbchoose').on('click', function(e) {
    	e.preventDefault();
    	var id = null;
    	fbphotoSelect(id);
    });
    // Set font
    $('#notePrev').addClass('justme');
    // Update Note Preview as user types
    $('#note').on('keyup', function() {
    	$('#notePrev').html( toHTML( $(this).val() ) );
    });
    // Upload new photo
    $('#btnUpload').on('click', self.save);
    // Delete a memory
    $('#btnErase').on('click', self.remove);
    // Prevent default on form submission
    $('form').on('submit', function() {
      return false;
    });
  };

  var uploadAlertTimeout;
  var hideAlert = function() {
    $("#formAlert").fadeOut(3000);
  };
  self.alert = function(type, message) {
    $('#formAlert').removeClass('alert-success alert-danger');
    $('#formAlert').addClass('alert alert-'+type).html(message).show();
    uploadAlertTimeout = setTimeout(hideAlert, 2000);
  };

  self.save = function() {
    memory.note = $('#note').val();
    var url = '/memories/';
    url += (memory.id === -1) ? "" : memory.id;
    $.post('/memories/', memory,  function(data) {
      asif = data;
      if (data.error) {
        self.alert("danger", data.error);
      }
      else {
        self.alert("success", data.message);
        self.reset();
      }
    });
  };

  self.remove = function() {
    var url = '/memories/';
    url += (memory.id === -1) ? "" : memory.id;
    var params = {
      id: memory.id,
      action: "DELETE"
    }
    $('#dvErase').modal('hide')
    $.post('/memories/', params,  function(data) {
      asif = data;
      if (data.error) {
        self.alert("danger", data.error);
      }
      else {
        self.alert("success", data.message);
        self.reset();
      }
    });
  }

  self.setPhoto = function(photo, copyFile) {
    memory.photo = photo || "";
    memory.copyFile = copyFile || false;
		$('#imgPrev').attr('src', memory.photo);
  };

  self.load = function(mem) {
    memory = (mem) ? new Memory(mem) : new Memory(memory);
    $('#note').val(memory.note);
    $('#notePrev').html(toHTML(memory.note));
    $('#imgPrev').attr('src', memory.photo);
    var buttonText = (memory.id === -1) ? "Upload" : "Update";
    $('#btnUpload').text(buttonText);
    $('#btnDelete').toggleClass('hidden', (memory.id === -1));
  };

  self.reset = function() {
    self.load({});
  };
};

var upload = new Upload();

upload.init();

// Navigation bindings
$('#mnuNewMemory').on('click', function(e) {
  e.preventDefault();
  showPage('newMemory');
});

$('#mnuMyMemories').on('click', function(e) {
  e.preventDefault();
  showPage('myMemories');
});

$('#mnuHome').on('click', function(e) {
  e.preventDefault();
  showPage('home');
});

var updateNav = function() {
  $('#mnuNewMemory, #mnuMyMemories').toggleClass('hidden', (typeof fbuser.id === "undefined"));
};

// Load Homepage on start
$(function() {
  $.cookie.json = true;
  var user = $.cookie('fbuser');
  if (typeof user === "undefined") {
    showPage('home');
  }
  else {
    fbuser = user;
    showPage('newMemory');
  }
  updateNav();
});

