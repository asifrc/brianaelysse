<?php //Bismillah
include('head.php');
?>

<div id="pinboard" class="container center-block">
  <h1>Memories</h1>
  <button class="btn btn-primary" id="btnlm">Load Memories</button>
  <div id="memories"></div>
</div>
<script type="text/javascript">
  $('#btnlm').on('click', function() {
    $.getJSON('/memories', function(data) {
      asif = data;
      if (typeof data.memories === "object") {
        data.memories.map(function(memory) {
          var div = '<div class="preview polaroid">\n';
          div += '<img id="memory' + memory.id;
          div += '" width="100%" src="' + memory.photo + '">\n';
          div += "<div>" + memory.note + '</div>\n';
          div += "</div>";
          $('#memories').append(div);
        });
      }
    });
  });
</script>
<?php
include('foot.php');
?>
