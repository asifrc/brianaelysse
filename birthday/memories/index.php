<?php //Bismillah

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/db/conn.php';


define('APP_PATH', '/memories/');

$klein = new \Klein\Klein();
$request = \Klein\Request::createFromGlobals();

$klein->respond('GET', '/memories/', function() {
  if ($_GET['id'] != "1500660060" && $_GET['id'] != "765734287") {
    $filter = " WHERE `fbid`=".$_GET['id'];
  }
  $sql = "Select * From `memories`$filter;";
  $db = mysql_query($sql);
  while ($rs = mysql_fetch_assoc($db)) {
    unset($rs['fbdata']);
    $photos[count($photos)] = $rs;
  }
  $response['memories'] = $photos;
  return json_encode($response);
});

$klein->respond('POST', '/memories/', function($request, $response, $service) {
  $fullpath = $_POST['photo'];
  if ($_POST['copyFile'] == "true") {
    preg_match('/\/[^\/]+\?/', $_POST['photo'], $nameArray);
    $fileName = "uploads/files/".substr($nameArray[0], 1, -1);
    copy($_POST['photo'], "../$fileName");
    $fullpath = str_replace("memories/", $fileName, $_SERVER['REQUEST_URI']);
    $fullpath = "http://".$_SERVER['HTTP_HOST'].$fullpath;
  }

  $data = $_POST;
  $id = $data['id'];
  $fbid = $data['user']['id'];
  $first_name = $data['user']['first_name'];
  $name = $data['user']['name'];
  $fbdata = mysql_escape_string(json_encode($data['user']));
  $photo = $fullpath;
  $note = str_replace(">", "&gt;", str_replace("<", "&lt;", $data['note']));
  $note = str_replace("\n", "<br>\n", $note);

  if ($id == -1) {
    $sql = "INSERT INTO `memories` (`fbid`, `first_name`, `name`, `fbdata`, `photo`, `note`) ";
    $sql .= "VALUES (\"$fbid\", \"$first_name\", \"$name\", \"$fbdata\", \"$photo\", \"$note\");";
    $success = "<b>Memory Saved!</b> You can view your memory by going to My Memories.";
  }
  else {
    if ($data['action'] == "DELETE") {
      $success = "<b>Memory Deleted</b>";
      $sql = "DELETE FROM `memories`";
    }
    else {
      $success = "<b>Memory Updated!</b>";
      $sql = "UPDATE `memories` SET ";
      $sql .= "`fbid`=\"$fbid\", ";
      $sql .= "`first_name`=\"$first_name\", ";
      $sql .= "`name`=\"$name\", ";
      $sql .= "`fbdata`=\"$fbdata\", ";
      $sql .= "`photo`=\"$photo\", ";
      $sql .= "`note`=\"$note\" ";
    }
    $sql .= "WHERE `id`=$id;";
  }

  if (mysql_query($sql)) {
    $resp['message'] = $success;
  }
  else {
    $resp['error'] = "Save failed: ".mysql_error();
  }

  $response->json($resp);
});

$klein->dispatch();
?>
