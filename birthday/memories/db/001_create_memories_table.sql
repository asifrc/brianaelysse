USE `asifgen`;

CREATE TABLE memories (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `fbid` text,
  `first_name` text,
  `name` text,
  `fbdata` longtext,
  `photo` text,
  `note` longtext,
  `ts` timestamp NOT NULL default CURRENT_TIMESTAMP,
  primary KEY(`id`)
);
