<?php //Bismillah
?>
<!DOCTYPE html>
<head>
<title>Briana's Birthday!!</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="./css/bootstrap.min.css">
<link rel="stylesheet" href="./css/csphotoselector.css">
<link href='http://fonts.googleapis.com/css?family=Just+Me+Again+Down+Here|Cedarville+Cursive' rel='stylesheet' type='text/css'>
<style type="text/css">
#uploadform, #pinboard
{
	padding-top: 60px;
}

#note
{
	width: 100%;
}

.polaroid
{
  width: 290px;
  padding: 15px;
  box-shadow: 3px 3px 10px #888888;
  margin: 15px;
  -webkit-backface-visibility: hidden;
}

.polaroid:hover {
  box-shadow: 3px 3px 20px #888888;
}

.rot-3 {
  transform: rotate(-3deg);
}
.rot-2 {
  transform: rotate(-2deg);
}
.rot-1 {
  transform: rotate(-1deg);
}
.rot0 {
  transform: rotate(-2deg);
}
.rot1 {
  transform: rotate(1deg);
}
.rot2 {
  transform: rotate(2deg);
}
.rot3 {
  transform: rotate(3deg);
}

#memories .polaroid {
  float: left;
}

.fileinput-button {
  position: relative;
  overflow: hidden;
}
.fileinput-button input {
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  opacity: 0;
  -ms-filter: 'alpha(opacity=0)';
  font-size: 200px;
  direction: ltr;
  cursor: pointer;
}

.polaroidMenu p {
  cursor: pointer;
  float: right;
  color: #666;
}

.polaroidMenu p:hover {
  color: #428bca;
}

/*
-----------
fonts
-----------
*/
.justme
{
	font-family: 'Just Me Again Down Here', cursive;
	font-size: 20px;
}
.cedar
{
	font-family: 'Cedarville Cursive', cursive;
}

/*
----------------------------------
Facebook Photoselector Repsonsive
----------------------------------
*/
@media (max-width: 767px)
{

	#CSPhotoSelector .CSPhotoSelector_dialog .CSAlbum_container .CSPhotoSelector_album {
		position: relative;
		display: inline-block;
		width: 100%;
		margin: 0 8px 0px;
		cursor: pointer;
		vertical-align: middle;
		border-bottom: 1px solid #e9e9e9;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog .CSAlbum_container .CSPhotoSelector_albumWrap {
		width: 58px;
		float: left;
		padding: 0px;
		border: 0px solid #ccc;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog .CSAlbum_container .CSPhotoSelector_albumWrap:hover {
		background-color: #fff;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog .CSAlbum_container .CSPhotoSelector_albumWrap div {
		height: 58px;
		overflow: hidden;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog .CSAlbum_container .CSPhotoSelector_album .CSPhotoSelector_photoAvatar {
		display: block;
		max-width: 140%;
		margin-top: auto;
		margin-bottom: auto;
		border: none;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog .CSPhoto_container .CSPhotoSelector_photo {
		margin: 3px;
		padding: 0px;
		height: 58px;
		width: 75px;
	}
	/* Album Title */
	#CSPhotoSelector .CSPhotoSelector_dialog .CSPhotoSelector_photosContainer .CSPhotoSelector_album .CSPhotoSelector_photoName {
		float: left;
		width: 200px;
		font-size: 16px;
		color: #696f7f;
		line-height: 18px;
		font-family: Helvetica, sans-serif;
		padding-top: 25px;
		padding-left: 10px;
	}
	#CSPhotoSelector .CSPhotoSelector_dialog {
		margin-top: 20px;
		height: 475px;
		width: 290px;
	}
	#CSPhotoSelector .CSPhotoSelector_content {
		height: 375px;
	}
	.CSPhotoSelector_content
	{
		width: 268px;
		margin: 0px;
	}
}

</style>
<script type="text/javascript" src="./js/jquery.min.js"></script>
<script type="text/javascript" src="./js/jquery.cookie.js"></script>
<script type="text/javascript" src="./js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript" src="./js/csphotoselector.js"></script>
<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.iframe-transport.js"></script>
<script src="js/jquery.fileupload.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script type="text/javascript">

/*---------*\
 * Routing *
\*---------*/

var currentPage = ".csel, .csel *";
var pageName = "home";

var showPage = function(page) {
  page = page || "home";
  pages = {};
  pages['newMemory'] = showForm;
  pages['editMemory'] = showForm;
  pages['myMemories'] = loadMemories;
  pages['home'] = showHome;

  if (page === "newMemory") {
    upload.reset();
    page = "editMemory";
  }

  if (page != pageName) {
    pageName = page;
    $('.navbar-nav *').removeClass('active');
    $(currentPage).fadeOut(400, function() {
      pages[page]();
    });
  }
  else {
    pages[page]();
  }
};

var showHome = function() {
  $('#mnuHome').addClass('active');
  currentPage = ".csel, .csel *";
  $(currentPage).fadeIn();
};

// Displays Upload Form
var showForm = function()
{
  $('#mnuNewMemory').addClass('active');
  currentPage = "#uploadform";
	$(currentPage).fadeIn();
};

// Load Memories and display as polaroids
var loadMemories = function() {
  $('#mnuMyMemories').addClass('active');
  currentPage = "#pinboard";
  $(currentPage).fadeIn();
  $.getJSON('/memories/?id='+fbuser.id, function(data) {
    asif = data;
    if (typeof data.memories === "object") {
      $('#memories').masonry();
      $('#memories').empty().masonry('destroy');
      $divs = data.memories.reduce(function(divs, memory) {
        var rotation = Math.floor(Math.random() * 7) - 3;
        var div = '<div class="preview polaroid rot' + rotation + ' ">\n';
        if (fbuser.id == memory.fbid) {
          div += '<div class="polaroidMenu">';
          div += '<p><span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit</p>';
          div += '</div>';
        }
        if (memory.photo !== "") {
          div += '<img id="memory' + memory.id;
          div += '" width="100%" src="' + memory.photo;
          div += '" title="' + memory.name + '">\n';
        }
        div += '<div class="justme">' + memory.note + '</div>\n';
        div += "</div>";
        $div = $(div);
        $div.appendTo('#memories');
        $div.find('.polaroidMenu p').on('click', function() {
          upload.load(memory);
          showPage('editMemory');
        });
        return divs.add($div);
      }, $(''));
      $('#memories').masonry({ itemSelector: '.polaroid' });
      $('#memories').masonry('addItems', $divs);
    }
  });
};

//CSPhotoSelector
var selector;

var selector, logActivity, callbackAlbumSelected, callbackPhotoUnselected, callbackSubmit;

/* --------------------------------------------------------------------
 * Photo selector functions
 * ----------------------------------------------------------------- */

function fbphotoSelect(id)
{
	var buttonOK = $('#CSPhotoSelector_buttonOK');

	// if no user/friend id is sent, default to current user
	if (!id) id = 'me';

	callbackAlbumSelected = function(albumId) {
		var album, name;
		album = CSPhotoSelector.getAlbumById(albumId);
		// show album photos
		selector.showPhotoSelector(null, album.id);
	};

	callbackAlbumUnselected = function(albumId) {
		var album, name;
		album = CSPhotoSelector.getAlbumById(albumId);
	};

	callbackPhotoSelected = function(photoId) {
		var photo;
		photo = CSPhotoSelector.getPhotoById(photoId);
		buttonOK.show();
	};

	callbackPhotoUnselected = function(photoId) {
		var photo;
		album = CSPhotoSelector.getPhotoById(photoId);
		buttonOK.hide();
	};

	callbackSubmit = function(photoId) {
		var photo;
		photo = CSPhotoSelector.getPhotoById(photoId);
    upload.setPhoto(photo.source, true);
	};


	// Initialise the Photo Selector with options that will apply to all instances
	CSPhotoSelector.init({debug: true});
	selector = CSPhotoSelector.newInstance({
		callbackAlbumSelected   : callbackAlbumSelected,
		callbackAlbumUnselected : callbackAlbumUnselected,
		callbackPhotoSelected   : callbackPhotoSelected,
		callbackPhotoUnselected : callbackPhotoUnselected,
		callbackSubmit          : callbackSubmit,
		maxSelection            : 1,
		albumsPerPage           : 5,
		photosPerPage           : 200,
		autoDeselection         : true
	});

	// reset and show album selector
	selector.reset();
	selector.showAlbumSelector(id);
}
</script>
</head>
<body>
<div id="fb-root"></div>
<script>

    // init the FB JS SDK
    FB.init({
      appId      : '227847900712091',                        // App ID from the app dashboard
      channelUrl : '//happybirthday.brianaelysse.com/fbchannel.php', // Channel file for x-domain comms
      status     : true,                                 // Check Facebook Login status
      xfbml      : true                                  // Look for social plugins on the page
    });

// Convert plaintext to html
function toHTML(txt)
{
	val = txt.replace(/</g, "&lt;").replace(/>/g, "&gt;");
	val = val.replace(/\n/g, "<br>\n");
	return val;
}
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56156321-1', 'auto');
  ga('send', 'pageview');

</script>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggle" data-toggle="collapse" data-target="navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="./" class="navbar-brand">Bree's Bday!</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="navbar-nav nav navbar-right">
					<li id="mnuNewMemory" class=""><a href="./">New Memory</a></li>
					<li id="mnuMyMemories" class=""><a href="./">My Memories</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- Begin Content -->
